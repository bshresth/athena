/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#include "AFP_DBTools/SiLocAlignDBTool.h"
#include "AFP_DBTools/SiGlobAlignDBTool.h"
#include "AFP_DBTools/SiAlignDBTester.h"

using namespace AFP;

DECLARE_COMPONENT(SiLocAlignDBTool)
DECLARE_COMPONENT(SiGlobAlignDBTool)
DECLARE_COMPONENT(SiAlignDBTester)
