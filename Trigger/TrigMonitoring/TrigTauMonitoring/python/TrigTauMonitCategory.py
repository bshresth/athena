# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

monitoring_singleTau = [
    'tau25_medium1_tracktwo'
]

monitoring_tau = [
    'tau0_perf_ptonly_L1TAU12',
    'tau0_perf_ptonly_L1TAU60',
    'tau25_idperf_track',
    'tau25_idperf_tracktwo',
    'tau25_perf_tracktwo',
    'tau25_medium1_tracktwo',
    'tau35_perf_tracktwo_tau25_perf_tracktwo',
    'tau35_medium1_tracktwo_tau25_medium1_tracktwo',
    'tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM',
    'mu14_iloose_tau25_perf_tracktwo',
    'tau160_idperf_track',
    'tau160_idperf_tracktwo',
    'tau160_perf_tracktwo',
    'tau160_medium1_tracktwo',
    'tau1_cosmic_track_L1MU4_EMPTY',
    'tau1_cosmic_ptonly_L1MU4_EMPTY',
    'tau8_cosmic_ptonly',
    'tau8_cosmic_track'
]

monitoring_tau_pp = [
    'tau0_ptonly_L1TAU100',
    'tau80_medium1_tracktwo_L1TAU60',
    'tau160_idperf_track_L1TAU100',
    'tau160_idperf_tracktwo_L1TAU100',
    'tau160_idperf_tracktwoEF_L1TAU100',
    'tau160_idperf_tracktwoMVA_L1TAU100',
    'tau160_idperf_tracktwoMVABDT_L1TAU100',
    'tau160_perf_track_L1TAU100',
    'tau160_perf_tracktwo_L1TAU100',
    'tau160_perf_tracktwoEF_L1TAU100',
    'tau160_perf_tracktwoMVA_L1TAU100',
    'tau160_perf_tracktwoMVABDT_L1TAU100',
    'tau160_mediumRNN_track_L1TAU100',
    'tau160_mediumRNN_tracktwo_L1TAU100',
    'tau160_mediumRNN_tracktwoEF_L1TAU100',
    'tau160_mediumRNN_tracktwoMVA_L1TAU100',
    'tau160_mediumRNN_tracktwoMVABDT_L1TAU100',
    'tau160_medium1_track_L1TAU100',
    'tau160_medium1_tracktwo_L1TAU100',
    'tau160_medium1_tracktwoEF_L1TAU100',
    'tau160_medium1_tracktwoMVA_L1TAU100',
    'tau160_medium1_tracktwoMVABDT_L1TAU100',
    'tau200_mediumRNN_track_L1TAU100',
    'tau200_mediumRNN_tracktwo_L1TAU100',
    'tau200_mediumRNN_tracktwoEF_L1TAU100',
    'tau200_mediumRNN_tracktwoMVA_L1TAU100',
    'tau200_mediumRNN_tracktwoMVABDT_L1TAU100',
    'tau200_medium1_track_L1TAU100',
    'tau200_medium1_tracktwo_L1TAU100',
    'tau200_medium1_tracktwoEF_L1TAU100',
    'tau200_medium1_tracktwoMVA_L1TAU100',
    'tau200_medium1_tracktwoMVABDT_L1TAU100', 
    # Will keep this commented out for now
    #'tau80_mediumRNN_tracktwoMVA_tau60_mediumRNN_tracktwoMVA_L1TAU60_2TAU40',
    #'tau80_mediumRNN_tracktwoMVA_tau35_mediumRNN_tracktwoMVA_L1TAU60_DR-TAU20ITAU12I',
    #'tau35_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_L1DR-TAU20ITAU12I-J25'    
]

monitoring_tau_validation = [

        # tau0
        'tau0_ptonly_L1TAU8',
        'tau0_ptonly_L1TAU60',
        # tau25
        'tau25_ptonly_L1TAU12IM',
        'tau25_idperf_track_L1TAU12IM',
        'tau25_idperf_tracktwo_L1TAU12IM',
        'tau25_idperf_tracktwoEF_L1TAU12IM',
        'tau25_idperf_tracktwoMVA_L1TAU12IM',
        'tau25_idperf_tracktwoMVABDT_L1TAU12IM',
        'tau25_perf_track_L1TAU12IM',
        'tau25_perf_tracktwo_L1TAU12IM',
        'tau25_perf_tracktwoEF_L1TAU12IM',
        'tau25_perf_tracktwoMVA_L1TAU12IM',
        'tau25_perf_tracktwoMVABDT_L1TAU12IM',
        'tau25_looseRNN_tracktwoMVA_L1TAU12IM',
        'tau25_looseRNN_tracktwoMVABDT_L1TAU12IM',       
        'tau25_mediumRNN_tracktwoMVA_L1TAU12IM',
        'tau25_mediumRNN_tracktwoMVABDT_L1TAU12IM',
        'tau25_tightRNN_tracktwoMVA_L1TAU12IM',
        'tau25_tightRNN_tracktwoMVABDT_L1TAU12IM',
        'tau25_medium1_track_L1TAU12IM',
        'tau25_medium1_tracktwo_L1TAU12IM',
        'tau25_medium1_tracktwoEF_L1TAU12IM',
        # tau35
        'tau35_ptonly_L1TAU12IM',
        'tau35_idperf_track_L1TAU12IM',
        'tau35_idperf_tracktwo_L1TAU12IM',
        'tau35_idperf_tracktwoEF_L1TAU12IM',
        'tau35_idperf_tracktwoMVA_L1TAU12IM',
        'tau35_idperf_tracktwoMVABDT_L1TAU12IM',
        'tau35_perf_track_L1TAU12IM',
        'tau35_perf_tracktwo_L1TAU12IM',
        'tau35_perf_tracktwoEF_L1TAU12IM',
        'tau35_perf_tracktwoMVA_L1TAU12IM',
        'tau35_perf_tracktwoMVABDT_L1TAU12IM',
        'tau35_looseRNN_tracktwoMVA_L1TAU12IM',
        'tau35_looseRNN_tracktwoMVABDT_L1TAU12IM',
        'tau35_mediumRNN_tracktwoMVA_L1TAU12IM',
        'tau35_mediumRNN_tracktwoMVABDT_L1TAU12IM',
        'tau35_tightRNN_tracktwoMVA_L1TAU12IM',
        'tau35_tightRNN_tracktwoMVABDT_L1TAU12IM',
        # tau160
        'tau160_ptonly_L1TAU100',
        'tau160_idperf_track_L1TAU100',
        'tau160_idperf_tracktwo_L1TAU100',
        'tau160_idperf_tracktwoEF_L1TAU100',
        'tau160_idperf_tracktwoMVA_L1TAU100',
        'tau160_idperf_tracktwoMVABDT_L1TAU100',
        'tau160_perf_track_L1TAU100',
        'tau160_perf_tracktwo_L1TAU100',
        'tau160_perf_tracktwoEF_L1TAU100',
        'tau160_perf_tracktwoMVA_L1TAU100',
        'tau160_perf_tracktwoMVABDT_L1TAU100',
        'tau160_mediumRNN_tracktwoMVA_L1TAU100',
        'tau160_mediumRNN_tracktwoMVABDT_L1TAU100',
        'tau160_medium1_track_L1TAU100',
        'tau160_medium1_tracktwo_L1TAU100',
        'tau160_medium1_tracktwoEF_L1TAU100',
]

monitoring_tau_cosmic = [
    'tau1_cosmic_track_L1MU4_EMPTY',
    'tau1_cosmic_ptonly_L1MU4_EMPTY',
    'tau8_cosmic_ptonly',
    'tau8_cosmic_track'
]


monitoring_singleTau_cosmic = 'tau8_cosmic_track'
